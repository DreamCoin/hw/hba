help(){
./hpsetup --help
}

force_reflash(){
  ./hpsetup -f
}

install(){
  wget $mirror
  rpm -ivh hp-firmware-smartarray-*.x86_64.rpm
  cd /usr/lib/x86_64-linux-gnu/hp-firmware-smartarray-*/
}

ark(){
  origin = 'https://downloads.hpe.com/pub/softlib2/software1/sc-linux-fw-array/p1156993750/v107486/hp-firmware-smartarray-14ef73e580-6.64-1.1.x86_64.rpm'
  mirror = '?'
}
